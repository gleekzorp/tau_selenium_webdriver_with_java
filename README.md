# TestAutomationUniversity Selenium WebDriver with Java

https://testautomationu.applitools.com/selenium-webdriver-tutorial-java/

## Resources

- [Course Code](https://github.com/angiejones/selenium-webdriver-java-course)
    - Tags for each chapter
- [Java](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [IntelliJ](https://www.jetbrains.com/idea/download/)
- [Chrome Browser](https://www.google.com/chrome/)
- [Chrome Driver Executable](http://chromedriver.chromium.org/downloads)
- [Maven Repository for Selenium Chrome Driver](https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-chrome-driver/3.141.59)
- [The Internet - Test Automation Practice Site](https://the-internet.herokuapp.com/)
- [Formy – Test Automation Practice Site](https://formy-project.herokuapp.com/form)
- [A Look at New Java Features in Test Automation](https://angiejones.tech/new-java-features-test-automation/)
- WAITS
  - [Timeouts JavaDoc](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/WebDriver.Timeouts)
  - [WebDriverWait JavaDoc](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/support/ui/WebDriverWait)
  - [ExpectedConditions JavaDoc](https://seleniumhq.github.io/selenium/docs/api/dotnet/html/T_OpenQA_Selenium_Support_UI_ExpectedConditions.htm)
     - [More ExpectedConditions](https://www.selenium.dev/selenium/docs/api/java/org/openqa/selenium/support/ui/ExpectedConditions.html)
  - [FluentWait JavaDoc](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/support/ui/FluentWait.html)
- JavaScript
  - [JavascriptExecutor JavaDoc](https://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/JavascriptExecutor.html)
  - [Automating in the Browser Using JavaScript](https://testautomationu.applitools.com/automating-in-the-browser-using-javascript/)

## Challenges

### Chapter 4 Challenge

> I wasn't able to fully test this as the EmailSentPage was getting an Internal 500 error.  See the open issue at https://github.com/saucelabs/the-internet/issues/68

- Go to the Forgot Password Page
- Enter an email address
  - Example: `tau@example.com`, it doesn't have to be a real email address
- Click the "Retrieve password" button and verify the new page says `Your e-mail's been sent!`
- Teachers Solution:
  - [ForgotPasswordPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/ForgotPasswordPage.java)
  - [EmailSentPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/5285af3211ee613b01e9be99bad485ef768ba3ce/webdriver_java/src/main/java/pages/EmailSentPage.java)
  - [ForgotPasswordTests](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/test/java/exercises/chapter4/passwords/ForgotPasswordTests.java)
  
### Chapter 6 Challenge

- Go to the Horizontal Slider Page
- Slide the slider to 4
- Verify the text equals 4
- Teachers Solution:
  - [HorizontalSliderPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/HorizontalSliderPage.java)
  - [SliderTests](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/test/java/exercises/chapter6/slider/SliderTests.java)
  
### Chapter 7 Challenge

- Go to the Context Menu Page
- Right-click on the box
- Verify the alert text is “You selected a context menu” and click OK
- Teacher Solution:
  - [ContextMenuPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/ContextMenuPage.java)
  - [ContextMenuTests](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/test/java/exercises/chapter7/contextmenu/ContextMenuTests.java)
  
### Chapter 8 Challenge

- Go to the Frames page
- Then go to the Nested Frames page
- Within a single test verify the text inside the left and bottom frames
- Teacher Solution:
  - [FramesPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/FramesPage.java)
  - [NestedFramesPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/NestedFramesPage.java)
  - [FramesTests](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/test/java/exercises/chapter8/frames/FrameTests.java)
  
### Chapter 9 Challenge

- Go to the Dynamic Loading Page
- Then click “Example 2” where the element is rendered after the fact
- Click on the start button
- Wait for the "Hello World!" to be available and verify the text
- Teacher Solution:
  - [DynamicLoadingPage](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/DynamicLoadingPage.java)
  - [DynamicLoadingExample2Page](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/main/java/pages/DynamicLoadingExample2Page.java)
  - [WaitTests](https://github.com/angiejones/selenium-webdriver-java-course/blob/master/webdriver_java/src/test/java/exercises/chapter9/wait/WaitTests.java)
  
### Chapter 10 Challenge

- Go to the Dropdown Page
- Using JavaScript change the dropdown to allow for multiple selections
- Select both Option 1 and Option 2
- Verify that you have those two selected, and that they're “Option 1” and “Option 2”

### Chapter 11 Challenge

- Click on the Dynamic Loading Link
- Simulate a Right Click on example 2 to open in a new tab
- Switch to the new tab and verify the "Start" button is there

### Chapter 14 Challenge

- Delete one of the cookies that https://the-internet.herokuapp.com/ sets
- Verify that cookie was deleted
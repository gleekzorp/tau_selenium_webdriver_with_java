// TODO:  If I pass a cookie name that doesn't exist we get a null pointer exception.  Should I worry about this?
package cookies;

import base.BaseTests;
import org.openqa.selenium.Cookie;

import org.testng.annotations.Test;
import utils.CookieManager;

import static org.testng.Assert.assertFalse;

public class CookiesTests extends BaseTests {

    @Test
    public void testDeleteCookie(){
        CookieManager cookieManager = getCookieManager();
        Cookie cookie = cookieManager.getCookieByName("optimizelyBuckets");
        cookieManager.deleteCookie(cookie);
        assertFalse(cookieManager.isCookiePresent(cookie), "Cookie was not deleted");
    }

    @Test
    public void testDeleteCookie2(){
        String cookieName = "optimizelyBuckets";
        CookieManager cookieManager = getCookieManager();
        cookieManager.deleteCookieByCookieName(cookieName);
        assertFalse(cookieManager.isCookiePresentByCookieName(cookieName), "Cookie was not deleted");
    }
}

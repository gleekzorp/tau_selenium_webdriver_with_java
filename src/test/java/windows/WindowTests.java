package windows;

import base.BaseTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WindowTests extends BaseTests {

    @Test
    public void testWindowTabs(){
        var examplePage2 = homePage.clickDynamicLoading().rightClickExample2();
        getWindowManager().switchToNewTab();
        assertTrue(examplePage2.isStartButtonDisplayed(), "Start button is not displayed");
    }
}

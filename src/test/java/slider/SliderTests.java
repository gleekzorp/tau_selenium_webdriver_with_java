package slider;

import base.BaseTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SliderTests extends BaseTests {

    @Test
    public void testSlider(){
        var sliderPage = homePage.clickHorizontalSlider();
        sliderPage.setSliderValue(4);
        assertEquals(sliderPage.getSliderValue(), "4", "Slider Value Incorrect");
    }

//    Creating a variable to use throughout.  You need to convert data types since getSlider returns a String
//    @Test
//    public void testSlider(){
//        int value = 4;
//        var sliderPage = homePage.clickHorizontalSlider();
//        sliderPage.setSliderValue(value);
//        assertEquals(Integer.parseInt(sliderPage.getSliderValue()), value, "Slider Value Incorrect");
//    }
}

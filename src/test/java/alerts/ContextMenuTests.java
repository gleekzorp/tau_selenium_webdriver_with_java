package alerts;

import base.BaseTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ContextMenuTests extends BaseTests {

    @Test
    public void testRightClick(){
        var contextMenuPage = homePage.clickContextMenu();
        contextMenuPage.rightClickHotSpotBox();
        String text = contextMenuPage.alert_getText();
        contextMenuPage.alert_clickAccept();
        assertEquals(text, "You selected a context menu", "Popup message incorrect");
    }
}

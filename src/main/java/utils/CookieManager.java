package utils;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.util.Set;

public class CookieManager {

    private WebDriver driver;

    public CookieManager(WebDriver driver) {
        this.driver = driver;
    }

    public Set<Cookie> getCookies(){
        return driver.manage().getCookies();
    }

    public Cookie getCookieByName(String cookieName){
        return driver.manage().getCookieNamed(cookieName);
    }

    public void deleteCookie(Cookie cookie){
        driver.manage().deleteCookie(cookie);
    }

    public void deleteCookieByCookieName(String cookieName){
        driver.manage().deleteCookieNamed(cookieName);
    }

    public boolean isCookiePresent(Cookie cookie){
        return driver.manage().getCookieNamed(cookie.getName()) != null;
    }

    public boolean isCookiePresentByCookieName(String cookieName){
        return driver.manage().getCookieNamed(cookieName) != null;
    }
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class ContextMenuPage {

    private WebDriver driver;
    private By hotSpotBox = By.id("hot-spot");

    public ContextMenuPage(WebDriver driver){
        this.driver = driver;
    }

    public void rightClickHotSpotBox(){
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(hotSpotBox)).contextClick().perform();
//        teacher solution passed the WebElement to the contextClick method
//        actions.contextClick(driver.findElement(hotSpotBox)).perform();
    }

    public String alert_getText(){
        return driver.switchTo().alert().getText();
    }

    public void alert_clickAccept(){
        driver.switchTo().alert().accept();
    }
}
